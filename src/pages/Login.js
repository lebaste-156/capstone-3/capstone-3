import { Button, Form } from 'react-bootstrap';
import { useState, useEffect } from 'react'
import Swal from 'sweetalert2'

export default function Login() {


	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('')

	const [ isFilled, setIsFilled ] = useState(false)

	useEffect(() => {
		if (email !== '' && password !== '') {
			setIsFilled(true);
		} else {
			setIsFilled(false);
		}
	}, [email, password])

	const LoginUser = async(eventSubmit) => {
		eventSubmit.preventDefault()

		fetch('https://agile-lowlands-37376.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(res => res.json()).then(data => {
			let token = data.access


			if(typeof token !== 'undefined') {
				localStorage.setItem('access', token);
				
				Swal.fire({
					icon: 'success',
					title: 'Login Successful',
					iconColor: 'black',
					confirmButtonColor: 'black'
				}).then(() => {
					window.location.href = '/'
				})
				
			} else {
				Swal.fire({
					icon: 'error',
					title: 'Login Failed',
					text: 'Check Email and Password',
					iconColor: 'red',
					confirmButtonColor: 'black'
				}).then(() => {
					window.location.href = '/login'
				})
			}
		})

		
				
		

		
	}

	return(
			<div className="sign-up-form">
				<h3 className="">Sign In</h3>
				<Form onSubmit={e => LoginUser(e)}>
					<Form.Group>
						<Form.Label className="form-label"> Email: </Form.Label>
						<Form.Control className="input-box" type="email" placeholder="Enter your Email" value={email} onChange={event => {setEmail(event.target.value)}} required />
					</Form.Group>

					<Form.Group>
						<Form.Label className="form-label"> Password: </Form.Label>
						<Form.Control className="input-box" type="password" placeholder="Enter your Password" value={password} onChange={event => {setPassword(event.target.value)}} required />
					</Form.Group>

					{
						isFilled ?
						<Button type="submit" className="button btn-light">
							Login
						</Button>
						:
						<Button className="button btn-light" disabled>
							Login
						</Button>
					}

				</Form>				
			</div>
	);
};
