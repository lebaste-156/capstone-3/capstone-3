import { Form, Button } from 'react-bootstrap'
import { useState, useContext } from 'react'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import { Navigate } from 'react-router-dom'

export default function CreateProduct() {
	const { user } = useContext(UserContext)
	const [ productName , setProductName ] = useState('')
	const [ productDescription , setProductDescription ] = useState('')
	const [ productPrice , setProductPrice ] = useState('')

	let token = localStorage.getItem('access');

	const createProduct = async(event) => {
		event.preventDefault()

		const isCreated = await fetch('https://agile-lowlands-37376.herokuapp.com/products/create', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
			    name: productName,
			    description: productDescription,
			    price: productPrice
			})
		}).then(res => res.json()).then(data => {
			if (data) {
				return true
			} else {
				return false
			}

		})

		if (isCreated) {
			Swal.fire({
				icon: 'success',
				title: 'Product Created Successfully',
				iconColor: 'black',
				confirmButtonColor: 'black',
			})
			window.location.href = '/admin/products'
		} else {
			Swal.fire({
				icon: 'error',
				title: 'Error in Creating Product',
				iconColor: 'red',
				confirmButtonColor: 'black',
			})
		}

	}

	return(
		user.isAdmin ?

			<div className="admin-create">
			<Form onSubmit={e => createProduct(e)}>
				<Form.Group>
					<Form.Label className=""> Product Name: </Form.Label>
					<Form.Control className="" type="text" placeholder="Product Name"  value={productName} onChange={e => {setProductName(e.target.value)}} required />
				</Form.Group>

				<Form.Group>
					<Form.Label className=""> Description: </Form.Label>
					<Form.Control className="" type="text" placeholder="Product Description" value={productDescription} onChange={e => {setProductDescription(e.target.value)}} required />
				</Form.Group>

				<Form.Group>
					<Form.Label className=""> Price: </Form.Label>
					<Form.Control className="" type="number" placeholder="Product Price" value={productPrice} onChange={e => {setProductPrice(e.target.value)}} required />
				</Form.Group>

				<Button type="submit" className="admin-create-btn button btn-light" > Create Product </Button>
			</Form>
			</div>
			:
			<Navigate to="/" />
	)
}