import {} from 'react-bootstrap';

export default function Banner() {
	return(
		<img src='/banner.jpg' alt="banner" className="banner w-100" />
	);
};