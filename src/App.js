import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { UserProvider } from './UserContext';
import { useState, useEffect } from 'react';

import AppNavBar from './components/AppNavBar';
import HomePage from './pages/Home';
import RegisterPage from './pages/Register';
import LoginPage from './pages/Login';
import LogoutPage from './pages/Logout';
import ProductsPage from './pages/Products';
import ProductView from './pages/ProductView';
import ContactUsPage from './pages/ContactUs';
import AllProducts from './pages/AllProducts';
import CreateProduct from './pages/CreateProduct';
import UpdateProduct from './pages/UpdateProduct';
import DeleteProduct from './pages/DeleteProduct';
import ArchiveProduct from './pages/ArchiveProduct';
import ActivateProduct from './pages/ActivateProduct';
import ErrorPage from './pages/Error';


import './App.css';

function App() {

  const [ user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear('access');
    setUser({
      id: null,
      isAdmin: null
    })
  };

  useEffect(() => {
    let token = localStorage.getItem('access');
    fetch('https://agile-lowlands-37376.herokuapp.com/users/user', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(res => res.json()).then(data => {
      if (typeof data._id !== 'undefined') {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>   
      <AppNavBar />  
        <Routes>
          <Route path="/" element={<><HomePage /></>} />
          <Route path="/register" element={<><RegisterPage /><AppNavBar /></>} />
          <Route path="/login" element={<><LoginPage /></>} />
          <Route path="/logout" element={<><LogoutPage /></>} />
          <Route path="/products" element={<><ProductsPage /></>} />
          <Route path="/products/view/:id" element={<><ProductView /></>} />
          <Route path="/contact-us" element={<><ContactUsPage /></>} />
          <Route path="/admin/products" element={<><AllProducts /></>} />
          <Route path="/admin/create" element={<><CreateProduct /></>} />
          <Route path="/admin/products/update/:id" element={<><UpdateProduct /></>} />
          <Route path="/admin/products/delete/:id" element={<><DeleteProduct /></>} />
          <Route path="/admin/products/archive/:id" element={<><ArchiveProduct /></>} />
          <Route path="/admin/products/unarchive/:id" element={<><ActivateProduct /></>} />
          <Route path="/*" element={<><ErrorPage /></>} />
        </Routes>
      </Router> 
    </UserProvider>
  );
}

export default App;
