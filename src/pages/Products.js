import { useState, useEffect } from 'react'
import ProductCard from '../components/ProductCard'

export default function Product() {
	const [ productCollection, setProductCollection ] = useState([])

	useEffect(() => {
		fetch('https://agile-lowlands-37376.herokuapp.com/products/active').then(res => res.json()).then(data => {
			setProductCollection(data.map(product => {
				return(
					<ProductCard key={product._id} productProp={product} />
				)
			}))
			
		})
	}, [productCollection])

	return(
		<div className="product-page">
			{productCollection}
		</div>	
	)
};