import { Navbar, Nav } from 'react-bootstrap';
import { NavLink, Link } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext'

export default function AppNavbar() {
	const {user} = useContext(UserContext);
	return(
			<Navbar className="navbar fixed-top" expand="lg">					
				<Navbar.Brand className="navbar text-white d-flex"><img className="logo" src="nezuko logo.png" alt="logo" /><Link to="/" className="navbar-brand text-white">
							Nezuko
						</Link>
				</Navbar.Brand>	
				<Navbar.Toggle aria-controls="basic-navbar-nav" className="bg-light"/>
				<Navbar.Collapse>
						<Nav className="nav ml-auto">
						<NavLink to="/" className="nav-link text-white">
							Home
						</NavLink>
						{
							user.isAdmin ?
							<NavLink to="/admin/products" className="nav-link text-white">
								Dashboard
							</NavLink>					
							:
							<NavLink to="/products" className="nav-link text-white">
								Products
							</NavLink>
						}

						{
							user.id !== null ?
							<NavLink to="/logout" className="nav-link text-white">
								Logout
							</NavLink>
							:
							<>
							<NavLink to="/register" className="nav-link text-white">
								Sign Up
							</NavLink>
							<NavLink to="/login" className="nav-link text-white">
								Sign In
							</NavLink>
							</>
						}									
					</Nav>
				</Navbar.Collapse>					
			</Navbar>
			
		
	);
};