import { Card, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom'



export default function ProductCard({productProp}) {
	return(
		<Col lg={3} md={4} sm={6} className="d-inline-flex">
			<Card>
				<Card.Body className="course-card">
					<Card.Title>
						{productProp.name}
					</Card.Title>
					<Card.Text>
						{productProp.description}
					</Card.Text>
					<Card.Text>
						Price: {productProp.price}
					</Card.Text>
					<Link to={`view/${productProp._id}`} className="btn btn-dark">
						View 
					</Link>		
				</Card.Body>
			</Card>		
		</Col>
		
		
	);
};